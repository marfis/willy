
def lex(code):
    result = []
    token = ""

    def push_token():
        nonlocal token
        if token != "" and not token.isspace():
            result.append(token)
            token = ""

    for c in code:
        if token.isalnum() and c.isalnum():
            pass
        elif token in ["<", ">", ":"] and c == "=":
            pass
        else:
            push_token()

        if not c.isspace():
            token += c

    push_token()

    result.append("$")

    return result

if __name__ == "__main__":
    print(lex("abc<35; x := 3 + 5"))
