#!/usr/bin/env python3

from lex import lex
import parse

def standard_context():
    return {
            "constants": { "true" : "Bool", "false" : "Bool" },
            "functions": {
                "*" : (["Int", "Int"], "Int"),
                "+" : (["Int", "Int"], "Int"),
                "-" : (["Int", "Int"], "Int"),
                "<" : (["Int", "Int"], "Bool"),
                ">" : (["Int", "Int"], "Bool"),
                "=" : (["Int", "Int"], "Bool"),
                "<=" : (["Int", "Int"], "Bool"),
                ">=" : (["Int", "Int"], "Bool"),
                "!" : (["Bool"], "Bool"),
                "&" : (["Bool", "Bool"], "Bool"),
                "|" : (["Bool", "Bool"], "Bool")
            },
            "function_table": {
                "*" : lambda a, b: a * b,
                "+" : lambda a, b: a + b,
                "-" : lambda a, b: a - b,
                "<" : lambda a, b: a < b,
                ">" : lambda a, b: a > b,
                "=" : lambda a, b: a == b,
                "<=" : lambda a, b: a <= b,
                ">=" : lambda a, b: a >= b,
                "!" : lambda a: not a,
                "&" : lambda a, b: a and b,
                "|" : lambda a, b: a or b
            },
            "variables": {}
        }

def run(code, variables, sigma):
    context = standard_context()
    context["variables"] = dict(context["variables"], **variables)
    program = parse.parse_program(lex(code), context)
    program.check_types(context)
    result = program.eval(context, sigma)
    for var, val in result.items():
        print("{var} = {val}".format(var=var, val=val))

def print_tree(code, variables, sigma):
    context = standard_context()
    context["variables"] = dict(context["variables"], **variables)
    program = parse.parse_program(lex(code), context)
    print("\\Tree {}".format(program.tree_string()))



if __name__ == "__main__":
    import sys
    args = sys.argv[1:]

    command_tree = False
    if len(args) > 0 and args[0] == "tree":
        command_tree = True
        args = args[1:]

    if len(args) > 0:
        file_name = args[0]
        args = args[1:]
    else:
        file_name = "-"

    variables = {}
    sigma = {}
    for arg in args:
        p = arg.split("=")
        if len(p) != 2:
            raise Exception("Unknown param " + arg)
        variables[p[0]] = "Int"
        sigma[p[0]] = int(p[1])

    if file_name == "-":
        code = sys.stdin.read()
    else:
        with open(file_name) as f:
            code = f.read()

    if command_tree:
        print_tree(code, variables, sigma)
    else:
        run(code, variables, sigma)
