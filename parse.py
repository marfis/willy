
class Expression:
    def eval(self, context, sigma):
        raise Exception("Not implemented")

    def get_type(self, context):
        return None

    def tree_string(self):
        raise Exception("Not implemented")


class ConstantExpression(Expression):

    def __init__(self, c):
        super().__init__()
        self.c = c

    def eval(self, context, sigma):
        if self.c.isdigit():
            return int(self.c)
        return context["constant_table"][self.c]

    def get_type(self, context):
        if self.c.isdigit():
            return "Int"
        return context["constants"][self.c]

    def tree_string(self):
        return self.c

class VarExpression(Expression):

    def __init__(self, v):
        super().__init__()
        self.v = v

    def eval(self, context, sigma):
        return sigma[self.v]

    def get_type(self, context):
        return context["variables"][self.v]

    def tree_string(self):
        return self.v

class FunctionExpression(Expression):

    def __init__(self, f, operands):
        super().__init__()
        self.f = f
        self.operands = operands

    def eval(self, context, sigma):
        return context["function_table"][self.f](*map(lambda o : o.eval(context, sigma), self.operands))

    def get_type(self, context):
        sig = context["functions"][self.f]
        if len(self.operands) != len(sig[0]):
            raise Exception("Function {} application wrong number of parameters {} / {}".format(self.f, len(sig[0]), len(self.operands)))
        for i, operand in enumerate(self.operands):
            if sig[0][i] != operand.get_type(context):
                raise Exception("Function {} application type #{} mismatch: {} / {}".format(self.f, i, sig[0][i], operand.get_type(context)))
        return sig[1]

    def tree_string(self):
        return "[.{{ {}({}) }} {} ]".format(self.f, ",".join(map(lambda o: "\\textit{a}", self.operands)), " ".join(map(lambda o: o.tree_string(), self.operands)))


def parse_expression(tokens, context, binding_power = 0):

    if tokens[0] == "(":
        tokens.pop(0)
        expression = parse_expression(tokens, context, binding_power = 0)
        if tokens.pop(0) != ")":
            raise Exception("Missing )")

    elif tokens[0].isdigit() or tokens[0] in context["constants"]:
        expression = ConstantExpression(tokens.pop(0))

    elif tokens[0] in context["variables"]:
        expression = VarExpression(tokens.pop(0))

    elif tokens[0] in context["functions"]:
        f = tokens.pop(0)
        if tokens.pop(0) != "(":
            raise Exception("Missign ( after function")
        operands = [parse_expression(tokens, context, binding_power = 0)]
        while tokens[0] == ",":
            tokens.pop(0)
            operands.append(parse_expression(tokens, context, binding_power = 0))
        if tokens.pop(0) != ")":
            raise Exception("Missing )")
        expression = FunctionExpression(f, operands)
    else:
        raise Exception("Unknown expression")

    # Infix operators
    infix = True
    while infix:
        infix = False
        if binding_power <= 10 and tokens[0] == "|":
            f = tokens.pop(0)
            right_expression = parse_expression(tokens, context, binding_power=20)
            expression = FunctionExpression(f, [expression, right_expression])
        if binding_power <= 20 and tokens[0] == "&":
            f = tokens.pop(0)
            right_expression = parse_expression(tokens, context, binding_power=30)
            expression = FunctionExpression(f, [expression, right_expression])
        elif binding_power <= 30 and tokens[0] in ["<", ">", "=", "<=", ">="]:
            f = tokens.pop(0)
            right_expression = parse_expression(tokens, context, binding_power=40)
            expression = FunctionExpression(f, [expression, right_expression])
            infix = True
        elif binding_power <= 40 and tokens[0] in ["+", "-"]:
            f = tokens.pop(0)
            right_expression = parse_expression(tokens, context, binding_power=50)
            expression = FunctionExpression(f, [expression, right_expression])
            infix = True
        elif binding_power <= 50 and tokens[0] in ["*"]:
            f = tokens.pop(0)
            right_expression = parse_expression(tokens, context, binding_power=60)
            expression = FunctionExpression(f, [expression, right_expression])
            infix = True

    return expression

class Statement:
    def eval_in_place(self, context, sigma):
        raise Exception("Not implemented")

    def eval(self, context, sigma):
        new_sigma = dict(sigma)
        self.eval_in_place(context, new_sigma)
        return new_sigma

    def check_types(self, context):
        pass

    def tree_string(self):
        raise Exception("Not implemented")

class SkipStatement(Statement):
    def eval_in_place(self, context, sigma):
        pass

    def tree_string(self):
        return "skip"

class AssignStatement(Statement):
    def __init__(self, v, x):
        super().__init__()
        self.v = v
        self.x = x

    def eval_in_place(self, context, sigma):
        sigma[self.v] = self.x.eval(context, sigma)

    def check_types(self, context):
        type_x = self.x.get_type(context)
        if context["variables"][self.v] == None:
            # Typify implicit declaration
            context["variables"][self.v] = type_x
        else:
            if context["variables"][self.v] != type_x:
                raise Exception("Assigment {} type mismatch {} / {}".format(self.v, context["variables"][self.v], type_x))

    def tree_string(self):
        return "[.{{\\textit{{v}} := \\textit{{a}}}} {} {} ]".format(self.v, self.x.tree_string())

class IfStatement(Statement):
    def __init__(self, condition, thenClause, elseClause):
        super().__init__()
        self.condition = condition
        self.thenClause = thenClause
        self.elseClause = elseClause

    def eval_in_place(self, context, sigma):
        b = self.condition.eval(context, sigma)
        if b:
            self.thenClause.eval_in_place(context, sigma)
        else:
            self.elseClause.eval_in_place(context, sigma)

    def check_types(self, context):
        type_c = self.condition.get_type(context)
        if type_c != "Bool":
            raise Exception("If condition must be Bool not {}".format(type_c))
        self.thenClause.check_types(context)
        self.elseClause.check_types(context)

    def tree_string(self):
        return "[.{{if (\\textit{{b}}) \\{{ \\textit{{S}} \\}} else \\{{ \\texit{{S}} \\}}}} {} {} {} ]".format(self.condition.tree_string(), self.thenClause.tree_string(), self.elseClause.tree_string())

class WhileStatement(Statement):
    def __init__(self, condition, body):
        super().__init__()
        self.condition = condition
        self.body = body

    def eval_in_place(self, context, sigma):
        b = self.condition.eval(context, sigma)
        while b:
            self.body.eval_in_place(context, sigma)
            b = self.condition.eval(context, sigma)

    def check_types(self, context):
        type_c = self.condition.get_type(context)
        if type_c != "Bool":
            raise Exception("While condition must be Bool not {}".format(type_c))
        self.body.check_types(context)

    def tree_string(self):
        return "[.{{while (\\textit{{b}}) \\{{ \\textit{{S}} \\}}}} {} {} ]".format(self.condition.tree_string(), self.body.tree_string())

class CompoundStatement(Statement):
    def __init__(self, statements):
        super().__init__()
        self.statements = statements

    def eval_in_place(self, context, sigma):
        for statement in self.statements:
            statement.eval_in_place(context, sigma)

    def check_types(self, context):
        for statement in self.statements:
            statement.check_types(context)

    def tree_string(self):
        def left_tree(statements):
            if len(statements) == 1:
                return statements[0].tree_string()
            else:
                return "[.{{(\\textit{{S}} ; \\textit{{S}})}} {} {} ]".format(left_tree(statements[0:-1]), statements[-1].tree_string())
        return left_tree(self.statements)

def parse_one_statement(tokens, context):
    if tokens[0] == "(":
        tokens.pop(0)
        statement = parse_statement(tokens, context)
        if tokens.pop(0) != ")":
            raise Exception("Missing ) after statements")
    elif tokens[0] == "skip":
        tokens.pop(0)
        statement = SkipStatement()
    elif tokens[0] == "if":
        tokens.pop(0)
        if tokens.pop(0) != "(":
            raise Exception("Missign ( after if")
        condition = parse_expression(tokens, context)
        if tokens.pop(0) != ")":
            raise Exception("Missign ) after if")
        if tokens.pop(0) != "{":
            raise Exception("Missign { after if")
        thenClause = parse_statement(tokens, context)
        if tokens.pop(0) != "}":
            raise Exception("Missign } after then clause")
        if tokens.pop(0) != "else":
            raise Exception("Missign else after then clause")
        if tokens.pop(0) != "{":
            raise Exception("Missign { after else")
        elseClause = parse_statement(tokens, context)
        if tokens.pop(0) != "}":
            raise Exception("Missign } after else clause")
        statement = IfStatement(condition, thenClause, elseClause)
    elif tokens[0] == "while":
        tokens.pop(0)
        if tokens.pop(0) != "(":
            raise Exception("Missign ( after while")
        condition = parse_expression(tokens, context)
        if tokens.pop(0) != ")":
            raise Exception("Missign ) after if")
        if tokens.pop(0) != "{":
            raise Exception("Missign { after while")
        body = parse_statement(tokens, context)
        if tokens.pop(0) != "}":
            raise Exception("Missign } after while body")
        statement = WhileStatement(condition, body)
    elif len(tokens) >= 2 and tokens[1] == ":=":
        if not tokens[0] in context["variables"]:
            context["variables"][tokens[0]] = None
        v = tokens.pop(0)
        tokens.pop(0)
        x = parse_expression(tokens, context)
        statement = AssignStatement(v, x)
    else:
        raise Exception("Unknown statement")

    return statement

def parse_statement(tokens, context):
    statements = [parse_one_statement(tokens, context)]
    while tokens[0] == ";":
        tokens.pop(0)
        statements.append(parse_one_statement(tokens, context))
    return CompoundStatement(statements)

def parse_program(tokens, context):
    statement = parse_statement(tokens, context)
    if not (len(tokens) == 1 and tokens[0] == "$"):
        raise Exception("Unknown remainder of program")
    tokens.pop(0)
    return statement

if __name__ == "__main__":
    from lex import lex
    context = {"constants": {}, "functions": {"*": 0, "+": 0, "-": 0}, "function_table":{"*" : lambda a, b: a * b, "+": lambda a, b: a + b, "-": lambda a, b: a - b}, "variables": {"a" : 0, "b": 0}}
    print(parse_expression(lex("1+(17+3)*(5+2)"), context).eval(context, {}))
    print(parse_expression(lex("+(21,3)"), context).eval(context, {}))
    print(parse_statement(lex("a := 3; b := *(a, 5)"), context).eval(context, {"a": 0, "b": 0}))
    print(parse_statement(lex("a := 3; if(a) { b := 1 } else {b:=2}"), context).eval(context, {"a": 0, "b": 0}))
    print(parse_statement(lex("a := 0; if(a) { b := 1 } else {b:=2}"), context).eval(context, {"a": 0, "b": 0}))
    print(parse_program(lex("a := 4; while(a){ b := b + a; a := a - 1 }"), context).eval(context, {"a": 0, "b": 0}))
